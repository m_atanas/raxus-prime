package com.softuni.raxus.screens;

import java.awt.Component;

import com.softuni.raxus.constants.Constants;

/**
 * This class is responsible for laying out the available options and forwarding
 * them appropriately so they are available after the game starts.
 * 
 * @author Marto
 * 
 */
public class OptionsScreen extends Screen {

	public OptionsScreen() {
		this(Constants.OPTIONS_SCREEN_TITLE);
	}

	public OptionsScreen(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Component createSpecificComponents() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void createScreen() {
		OptionsScreen optionsScreen = new OptionsScreen();
	}

	@Override
	public void destroyScreen() {
		dispose();
	}
}
